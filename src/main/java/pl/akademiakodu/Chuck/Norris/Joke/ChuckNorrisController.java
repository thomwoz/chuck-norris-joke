package pl.akademiakodu.Chuck.Norris.Joke;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ChuckNorrisController {

    ChuckNorrisQuotes chuckNorrisQuotes = new ChuckNorrisQuotes();

    @GetMapping("/")
    public String getChuckJoke(ModelMap modelMap){
        modelMap.put("chuckJoke", chuckNorrisQuotes.getRandomQuote());
        return "chuck";
    }
}
